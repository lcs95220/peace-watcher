import java.time.LocalDate

import Main.genReports
import com.projet.peaceWatcherSimulateur.utils.ReportUtils

import math.{pow, sqrt}

object Main {

  def main(args: Array[String]): Unit = {
    (1 to 50).foreach(n => {
      genReports(args(0))
      println("Report n° "+n)
    })
  }

  def genReports(kafkaAddress: String): Unit = {
    val words = getRandomWords("data/DicoExpression.csv",5)
    val wordsSum = words.map(b => b(1).toInt).sum

    val behaviors = getRandomWords("data/DicoComportement.csv",3)
    val behaviorsSum = behaviors.map(b => b(1).toInt).sum

    val person = getRandomWords("data/population.csv", 1).head

    val r = scala.util.Random
    val rage = behaviorsSum + wordsSum //generation d'une rage initiale basée sur les mots entendus et le comportement
    val pos = (r.nextFloat * 100, r.nextFloat * 100) //""        des coordonnées du rapport
    val hour = (r.nextInt(24), r.nextInt(60)) //  ""          de l'heure du rapport

    /*println("Rage initiale: " + rage)
    println("Localisation: " + pos)
    println("Heure: " + hour._1 + ":" + hour._2)*/
    val peaceScore = (rage * genCoefDistance(pos._1,pos._2) * genCoefHour(hour)*genCoefAge(getAgeFromPersonFromCSV(person))) * 100 / 25
    val allWords = words.map(w => w(0))
    val allBehaviors = behaviors.map(b => b(0))
    val report = ReportUtils.Report("0",hour,pos,person(1),allWords,allBehaviors,peaceScore.toInt)
    val reportFormat = ReportUtils.reportToStreamFormat(report)
    println(kafkaAddress+":9092")
    kafka.ProducerKafka.writeToKafka(kafkaAddress+":9092","peaceland",reportFormat)
    println(reportFormat)
  }


  def getAgeFromPersonFromCSV(attributes:Array[String]): Int ={
    LocalDate.now.getYear-attributes(0).toInt
  }

  def genCoefAge(age:Int):Double={
      if(15< age && age <20){1.20}
      else if(20<age && age <30){1.40}
      else if( age > 65 ){0.70}
      else 1.0
  }

  def genCoefHour(hour: (Int, Int)): Double = {
    if ((hour._1 >= 6 && hour._1 <= 9) || hour._1 >= 16 && hour._1 <= 19) {
      1.20
    }
    else if (hour._1 >= 0 && hour._1 <= 3) {
      1.30
    } else {
      1.0
    }

  }

  //calcul d'un coefficient multiplicateur de la rage ( on considère qu'être au centre multiplie par deux l'enervement)
  //donc plus on est au centre plus on est énervé ( coef >1 ) plus on est loin moins on l'est (coef < 1)
  def genCoefDistance(pos:(Float, Float)):Double={
    val center = (50,50)
    val distance = sqrt(pow((pos._1 - center._1),2) + pow((pos._2 - center._2),2)) //calcul de la distance par rapport au centre de la carte
    // println("Distance: " + distance)
    2 - distance / 50
  }


  def getRandomWords(path: String, nbWords: Int): List[Array[String]] = {
    val r = scala.util.Random
    val bufferedSource = io.Source.fromFile(path)
    val dictionnary = bufferedSource.getLines().toList
    val lenght = dictionnary.length
    val indexWords = Seq.fill(nbWords)(r.nextInt(lenght))
    indexWords.map(index => dictionnary(index).split(";")).toList
  }
}

