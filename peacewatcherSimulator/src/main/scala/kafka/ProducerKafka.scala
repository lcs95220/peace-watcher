package kafka
import java.util
import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

object ProducerKafka {

  def getKafkaProducerParams(kafkaBootstrapServer:String) : Properties ={
    val props = new Properties
    props.put("key.serializer", Class.forName("org.apache.kafka.common.serialization.StringSerializer"))
    props.put("value.serializer",Class.forName("org.apache.kafka.common.serialization.StringSerializer"))
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer","org.apache.kafka.common.serialization.StringSerializer")
    props.put("acks","all") //définit si l'on attends un accusé de reception (all signfie systematiquement)
    props.put("bootstrap.servers",kafkaBootstrapServer)
    props
  }


  def writeToKafka(kafkaBootstrapServer: String, topic_name: String, message: String): Unit = {
      System.setProperty("java.security.auth.login.config", "K:/PROJET_DEV/kafka_2.11-0.9.0.0/kafka_2.11-0.9.0.0/config/jass.conf")
      val producer = new KafkaProducer[String,String](getKafkaProducerParams(kafkaBootstrapServer))
      val record = new ProducerRecord[String,String](topic_name,message)
      producer.send(record)
      producer.close()
  }
}
