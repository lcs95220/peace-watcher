package com.projet.peaceWatcherSimulateur.utils
object ReportUtils {
  case class Report (
                      id : String,
                      hour: (Int,Int),
                      position: (Float,Float),
                      name : String,
                      words : List[String],
                      behaviors: List[String],
                      peaceScore : Int
                   )

  def reportToStreamFormat(report: ReportUtils.Report): String = {
    "id:"+report.id+";"+
      "hour:"+report.hour._1 + "H" + report.hour._2+";"+
      "position:"+report.position._1 + "," + report.position._2+";"+
      "name:"+report.name+";"+
      "words:"+report.words.foldLeft("")((w1,w2) => w1 + "," +w2)+";"+
      "behaviors:" + report.behaviors.foldLeft("")((b1,b2) => b1 + "," +b2)+";"+
      "peaceScore:" + report.peaceScore+";"
  }

}