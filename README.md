**PROJET_SCALA**

MEMBRES:

-   HILAIRE Lucas
-   BERNARD Bastien
-   LOUISON Jean-Max
-   DOMERGUE Camille

Pour lancer le sereur kafka (après cd dans le dossier) -> chaque commande dans une terminal

-   $ bin/zookeeper-server-start.sh config/zookeeper.properties
-   $ bin/kafka-server-start.sh config/server.properties

--> lors de la première fois créer le topic désirer avec la commande
$ bin/kafka-topics.sh --create --topic TestTopic --bootstrap-server localhost:9092

Sur windows remplacer:

-   bin/ par .\bin\windows\ --> choisir les .bat
-   config par .\config

Pour vérifier que le consommateur fonctionne correctement on peut lancer un consommateur avec ( toujours dans un autre terminal) :

-   $ bin/kafka-console-consumer.sh --topic TestTopic --from-beginning --bootstrap-server localhost:9092

Si pb lors du lancement de la commande bin/kafka-server-start.sh config/server.properties, aller dans le fichier config/server.properties :

-   Aller à la ligne commentée listeners = PLAINTEXT://host_name:9092, la décommenter et mettre listeners = PLAINTEXT://localhost:9092
