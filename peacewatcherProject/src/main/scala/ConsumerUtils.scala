
import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecord}
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Seconds, StreamingContext}

object ConsumerUtils {
    //val db_uri="mongodb://127.0.0.1:27017/peaceland.report"
    val topics="peaceland"
    //val brokers = "localhost:9092"

    def initSparkSession(db_uri: String): SparkSession ={
      SparkSession.builder()
        .master("local")
        .appName("MongoSparkConnectorIntro")
        .config("spark.mongodb.input.uri", db_uri)
        .config("spark.mongodb.output.uri", db_uri)
        .getOrCreate()
    }

  def initStreamingContext(): StreamingContext={
    val SparkConf = new SparkConf().setMaster("local[*]").setAppName("KafkaStreaming")
    new StreamingContext(SparkConf, Seconds(3))
  }

  def createDirectStream(groupId:String,ssc:StreamingContext, brokers: String):InputDStream[ConsumerRecord[String, String]]={
    val topicSet = topics.split(",").toSet
    val kafkaParams = Map[String,Object](
      ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG->brokers,
      ConsumerConfig.GROUP_ID_CONFIG->groupId,
      ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG->"org.apache.kafka.common.serialization.StringDeserializer",
      ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG->"org.apache.kafka.common.serialization.StringDeserializer",
    )

    KafkaUtils.createDirectStream[String,String](
      ssc,LocationStrategies.PreferConsistent, ConsumerStrategies.Subscribe[String,String](topicSet,kafkaParams)
    )
    }
}
