import com.mongodb.spark.MongoSpark
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Seconds, StreamingContext}

object DataBaseConsumer {
  def main(args: Array[String]): Unit = {
    val db_uri = "mongodb://"+args(1)+":27017/peaceland.report"    
    val groupid = "DB_CONSUMER"

    val ssc = ConsumerUtils.initStreamingContext()
    val sc = ssc.sparkContext
    sc.setLogLevel("OFF")
    val spark = ConsumerUtils.initSparkSession(db_uri)

    val messages = ConsumerUtils.createDirectStream(groupid,ssc, args(0)+":9092")
    val line =messages.map(_.value)

    val reports = line.map {
      _.split(";")
        .map(_.split(":"))
        .map {
          case Array(_,v) => v
        }
    }
    reports.foreachRDD(rdd => {
      import spark.implicits._
      val reports = rdd.map(value => Report(value(0), value(1), value(2), value(3), value(4), value(5), value(6))).toDF()
      MongoSpark save
      reports.write
        .option("spark.mongodb.output.uri", db_uri)
        .mode("append")
    })

    messages.map(_.value).print()
    ssc.start()
    ssc.awaitTermination()
  }
  case class Report (
                      id : String,
                      hour: String,
                      position: String,
                      name : String,
                      words : String,
                      behaviors: String,
                      peaceScore : String
                    )
}
