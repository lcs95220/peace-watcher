import com.mongodb.spark.MongoSpark
import com.mongodb.spark.config.ReadConfig
import org.apache.spark.sql.functions.{avg, desc, regexp_replace}

import org.apache.spark.sql.{Dataset, SparkSession}
import org.apache.spark.sql.functions.{max,avg}

object Stats {
  def main(args: Array[String]): Unit = {
    val db_uri = "mongodb://"+args(0)+":27017"
    println("URI mongo : "+db_uri)
    val spark = initSparkMongo(db_uri)
    println("Tous les citoyens et leur nombre d'avertissements (peaceScore >= 50) : ")
    getWarningReportsCountByUser(spark, db_uri).show()
    //println("Citoyen le plus dangereux : ")
    //getMostDangerousCitizen(spark, db_uri).foreach(println(_))
    /*getAverageAge(spark, db_uri).show()

    println("Position moyenne des avertissements ")
    getAveragePositionOfWarnings(spark, db_uri).show()

    getAVGReportsByHours(spark,6,14,db_uri).show()
    getAVGReportsByHours(spark,14,22,db_uri).show()
    getAVGReportsByHours(spark,22,6,db_uri).show()*/
    spark.close()
  }

  def getAverageAge(spark: SparkSession, db_uri: String) = {
    val readConfigUser = {
      ReadConfig(Map("uri" -> db_uri, "database" -> "peaceland", "collection" -> "user"), Some(ReadConfig(spark)))
    }
    val users = MongoSpark.load(spark,readConfigUser)
    import spark.implicits._
    val names = getWarningReportsCountByUser(spark, db_uri)
      .map(warning => warning.getString(0)).collectAsList
    users.filter(user => names.contains(user.getString(1)))
      .agg(avg("year").as("Moyenne année de naissance des citoyens les plus dangereux"))
  }

  def getWarningReportsCountByUser(spark: SparkSession, db_uri: String) = getAllWarningReports(spark, db_uri).groupBy("name").count().sort(desc("count"))

  def initSparkMongo(db_uri: String): SparkSession = {
    val db_full_uri = db_uri+"/peaceland.report"
      SparkSession.builder()
      .master("local")
      .appName("MongoSparkConnectorIntro")
      .config("spark.mongodb.input.uri", db_full_uri)
      .config("spark.mongodb.output.uri", db_full_uri)
      .getOrCreate()
  }

  def getAveragePositionOfWarnings(spark:SparkSession, db_uri: String)={
    import spark.implicits._
     getAllWarningReports(spark, db_uri).map(report=>report.position.split(",")).map{case Array(x,y)=> (x,y)}.toDF("x","y").agg(avg("x").as("x"),avg("y").as("y"))
  }

  def getMostDangerousCitizen(spark: SparkSession, db_uri: String) = getWarningReportsCountByUser(spark, db_uri).head(1)

  def getAllWarningReports(spark: SparkSession, db_uri: String): Dataset[Report] = {
    val readConfigReport = {
      ReadConfig(Map("uri" -> db_uri, "database" -> "peaceland", "collection" -> "report"), Some(ReadConfig(spark)))
    }
    val rdd = MongoSpark.load(spark,readConfigReport)
    import spark.implicits._
    rdd.map(report => Report(report.getAs("id"),report.getAs("hour"),report.getAs("position"),report.getAs("name"),report.getAs("words"),report.getAs("behaviors"),report.getAs("peaceScore")))
      .filter(report => {
        report.peaceScore.toInt >= 50
      })
  }


  def getReportsByHours(spark: SparkSession, from:Int, to:Int, db_uri: String)={
    import spark.implicits._
    getAllWarningReports(spark, db_uri)
      .map(_.hour.split("H"))
      .map{
        case Array(h,_) => h.toInt
      }
      .filter(hour=>{
        if(from > to)
          hour>=from && hour<=0 || hour>=0 && hour<=to
        else
          hour>=from && hour<=to
      })
  }

  def getAVGReportsByHours(spark: SparkSession, from:Int, to:Int, db_uri: String)={
    println("Heure Moyenne d'enervement entre "+from+"H et "+to+"H")
    getReportsByHours(spark, from,to, db_uri)
      .toDF("hour")
      .agg(avg("hour").as("Pic d'enervement"))
  }


    case class Report (
                      id : String,
                      hour: String,
                      position: String,
                      name : String,
                      words : String,
                      behaviors: String,
                      peaceScore : String
                    )
}
